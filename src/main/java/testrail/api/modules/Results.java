package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;


@Data
public class Results {

    @SerializedName("results")
    @Expose
    private List<Result> results;
}
