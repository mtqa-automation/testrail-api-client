package unitTest;

import java.util.List;
import java.util.ListIterator;

import org.testng.annotations.Test;

import testrail.api.functions.TestRail;
import testrail.api.functions.TestRail.TestStatus;
import testrail.api.modules.Project;

public class SampleTest {
	TestRail testRail;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SampleTest st=new SampleTest();
		/*
		 * st.Addproject(); st.AddMileStone(); st.AddTestPlan();
		 * st.AddTestPlanEntry(); st.AddtestResult();
		 */
		List<Project> list = st.testRail.GetProjects();
		ListIterator<Project> iterator = list.listIterator();
		while(iterator.hasNext())
		System.out.println(iterator.next().getName());
	}

	@Test
	public void JenkinTest()
	{
		SampleTest st=new SampleTest();
		List<Project> list = st.testRail.GetProjects();
		ListIterator<Project> iterator = list.listIterator();
		while(iterator.hasNext())
		System.out.println(iterator.next().getName());

	}
	public SampleTest()
	{
		testRail= TestRail.GetInstance("http://vlfcrailsp01","smallika","Gouti@11");
	}
	
	public void Addproject()
	{
		testRail.AddProject("Project1","Added project for testing");
		System.out.println("projectID:"+testRail.GetProjectID("Project1"));
	}
	public void AddMileStone()
	{
		testRail.AddMileStone("Project1","Milestone2","Added Milestone for testing");
		System.out.println("Milestone2:"+testRail.GetMilestoneID("Project1","Milestone2"));
	}
	public void AddTestPlan()
	{
		testRail.AddTestPlan("Project1","Milestone2", "TestPlan2", "Added Testplan for testing");
		
	}
	public void AddTestPlanEntry()
	{
		testRail.AddNewPlanEntry("Project1", "TestPlan2", "Smoke","C1","sidu@gmail.com");
		System.out.println("Run ID: "+testRail.GetPlanRun("Project1", "TestPlan2","Smoke").getId());
	}
	public void AddtestResult()
	{
		testRail.AddTestResultWithCase("Project1", "TestPlan2", "Smoke","C1,C1,C1,C1",TestStatus.BLOCK,"Test verified and passed with date 21 Feb","sidu@gmail.com");
	}
	
}
