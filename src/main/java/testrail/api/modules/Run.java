package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;


@Data
public class Run {

    @SerializedName("assignedto_id")
    @Expose
    private Integer assignedtoId;
    public Integer getAssignedtoId() {
		return assignedtoId;
	}
	public void setAssignedtoId(Integer assignedtoId) {
		this.assignedtoId = assignedtoId;
	}
	public Integer getBlockedCount() {
		return blockedCount;
	}
	public void setBlockedCount(Integer blockedCount) {
		this.blockedCount = blockedCount;
	}
	public Object getCompletedOn() {
		return completedOn;
	}
	public void setCompletedOn(Object completedOn) {
		this.completedOn = completedOn;
	}
	public String getConfig() {
		return config;
	}
	public void setConfig(String config) {
		this.config = config;
	}
	public List<Integer> getConfigIds() {
		return configIds;
	}
	public void setConfigIds(List<Integer> configIds) {
		this.configIds = configIds;
	}
	public Integer getCustomStatus1Count() {
		return customStatus1Count;
	}
	public void setCustomStatus1Count(Integer customStatus1Count) {
		this.customStatus1Count = customStatus1Count;
	}
	public Integer getCustomStatus2Count() {
		return customStatus2Count;
	}
	public void setCustomStatus2Count(Integer customStatus2Count) {
		this.customStatus2Count = customStatus2Count;
	}
	public Integer getCustomStatus3Count() {
		return customStatus3Count;
	}
	public void setCustomStatus3Count(Integer customStatus3Count) {
		this.customStatus3Count = customStatus3Count;
	}
	public Integer getCustomStatus4Count() {
		return customStatus4Count;
	}
	public void setCustomStatus4Count(Integer customStatus4Count) {
		this.customStatus4Count = customStatus4Count;
	}
	public Integer getCustomStatus5Count() {
		return customStatus5Count;
	}
	public void setCustomStatus5Count(Integer customStatus5Count) {
		this.customStatus5Count = customStatus5Count;
	}
	public Integer getCustomStatus6Count() {
		return customStatus6Count;
	}
	public void setCustomStatus6Count(Integer customStatus6Count) {
		this.customStatus6Count = customStatus6Count;
	}
	public Integer getCustomStatus7Count() {
		return customStatus7Count;
	}
	public void setCustomStatus7Count(Integer customStatus7Count) {
		this.customStatus7Count = customStatus7Count;
	}
	public Object getDescription() {
		return description;
	}
	public void setDescription(Object description) {
		this.description = description;
	}
	public String getEntryId() {
		return entryId;
	}
	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}
	public Integer getEntryIndex() {
		return entryIndex;
	}
	public void setEntryIndex(Integer entryIndex) {
		this.entryIndex = entryIndex;
	}
	public Integer getFailedCount() {
		return failedCount;
	}
	public void setFailedCount(Integer failedCount) {
		this.failedCount = failedCount;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Boolean getIncludeAll() {
		return includeAll;
	}
	public void setIncludeAll(Boolean includeAll) {
		this.includeAll = includeAll;
	}
	public List<Integer> getCaseIds() {
		return caseIds;
	}
	public void setCaseIds(List<Integer> caseIds) {
		this.caseIds = caseIds;
	}
	public Boolean getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public Integer getMilestoneId() {
		return milestoneId;
	}
	public void setMilestoneId(Integer milestoneId) {
		this.milestoneId = milestoneId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPassedCount() {
		return passedCount;
	}
	public void setPassedCount(Integer passedCount) {
		this.passedCount = passedCount;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getRetestCount() {
		return retestCount;
	}
	public void setRetestCount(Integer retestCount) {
		this.retestCount = retestCount;
	}
	public Integer getSuiteId() {
		return suiteId;
	}
	public void setSuiteId(Integer suiteId) {
		this.suiteId = suiteId;
	}
	public Integer getUntestedCount() {
		return untestedCount;
	}
	public void setUntestedCount(Integer untestedCount) {
		this.untestedCount = untestedCount;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@SerializedName("blocked_count")
    @Expose
    private Integer blockedCount;
    @SerializedName("completed_on")
    @Expose
    private Object completedOn;
    @SerializedName("config")
    @Expose
    private String config;
    @SerializedName("config_ids")
    @Expose
    private List<Integer> configIds = null;
    @SerializedName("custom_status1_count")
    @Expose
    private Integer customStatus1Count;
    @SerializedName("custom_status2_count")
    @Expose
    private Integer customStatus2Count;
    @SerializedName("custom_status3_count")
    @Expose
    private Integer customStatus3Count;
    @SerializedName("custom_status4_count")
    @Expose
    private Integer customStatus4Count;
    @SerializedName("custom_status5_count")
    @Expose
    private Integer customStatus5Count;
    @SerializedName("custom_status6_count")
    @Expose
    private Integer customStatus6Count;
    @SerializedName("custom_status7_count")
    @Expose
    private Integer customStatus7Count;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("entry_id")
    @Expose
    private String entryId;
    @SerializedName("entry_index")
    @Expose
    private Integer entryIndex;
    @SerializedName("failed_count")
    @Expose
    private Integer failedCount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("include_all")
    @Expose
    private Boolean includeAll;
    @SerializedName("case_ids")
    @Expose
    private List<Integer> caseIds;
    @SerializedName("is_completed")
    @Expose
    private Boolean isCompleted;
    @SerializedName("milestone_id")
    @Expose
    private Integer milestoneId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("passed_count")
    @Expose
    private Integer passedCount;
    @SerializedName("plan_id")
    @Expose
    private Integer planId;
    @SerializedName("project_id")
    @Expose
    private Integer projectId;
    @SerializedName("retest_count")
    @Expose
    private Integer retestCount;
    @SerializedName("suite_id")
    @Expose
    private Integer suiteId;
    @SerializedName("untested_count")
    @Expose
    private Integer untestedCount;
    @SerializedName("url")
    @Expose
    private String url;
}
