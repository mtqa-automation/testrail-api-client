package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;


@Data
public class Test {

   
	@SerializedName("assignedto_id")
    @Expose
    private int assignedtoId;
    @SerializedName("case_id")
    @Expose
    private int caseId;
    @SerializedName("custom_expected")
    @Expose
    private String customExpected;
    @SerializedName("custom_preconds")
    @Expose
    private String customPreconds;
    @SerializedName("custom_steps_separated")
    @Expose
    private List<CustomStepsSeparated> customStepsSeparated = null;
    @SerializedName("estimate")
    @Expose
    private String estimate;
    @SerializedName("estimate_forecast")
    @Expose
    private Object estimateForecast;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("priority_id")
    @Expose
    private int priorityId;
    @SerializedName("run_id")
    @Expose
    private int runId;
    @SerializedName("status_id")
    @Expose
    private int statusId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type_id")
    @Expose
    private int typeId;
	public int getAssignedtoId() {
		return assignedtoId;
	}
	public int getCaseId() {
		return caseId;
	}
	public String getCustomExpected() {
		return customExpected;
	}
	public String getCustomPreconds() {
		return customPreconds;
	}
	public List<CustomStepsSeparated> getCustomStepsSeparated() {
		return customStepsSeparated;
	}
	public String getEstimate() {
		return estimate;
	}
	public Object getEstimateForecast() {
		return estimateForecast;
	}
	public int getId() {
		return id;
	}
	public int getPriorityId() {
		return priorityId;
	}
	public int getRunId() {
		return runId;
	}
	public int getStatusId() {
		return statusId;
	}
	public String getTitle() {
		return title;
	}
	public int getTypeId() {
		return typeId;
	}
}
