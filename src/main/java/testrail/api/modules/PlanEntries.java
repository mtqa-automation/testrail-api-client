package testrail.api.modules;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlanEntries {
	 public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getSuite_id() {
		return suite_id;
	}

	public void setSuite_id(int suite_id) {
		this.suite_id = suite_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRefs() {
		return refs;
	}

	public void setRefs(String refs) {
		this.refs = refs;
	}

	public Object getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isInclude_all() {
		return include_all;
	}

	public void setInclude_all(boolean include_all) {
		this.include_all = include_all;
	}

	public List<Run> getRuns() {
		return runs;
	}

	public void setRuns(List<Run> runs) {
		this.runs = runs;
	}

	@SerializedName("id")
	    @Expose
	    private String id;
	 
	 @SerializedName("suite_id")
	    @Expose
	    private Integer suite_id;
	 
	 @SerializedName("name")
	    @Expose
	    private String name;
	 
	 @SerializedName("refs")
	    @Expose
	    private String refs;
	 
	 @SerializedName("description")
	    @Expose
	    private String description;
	 
	 @SerializedName("include_all")
	    @Expose
	    private boolean include_all;
	 
	 @SerializedName("runs")
	    @Expose
	    private List<Run> runs=null;
	 

 
	 @SerializedName("case_ids")
	    @Expose
	    private List<Integer> case_ids;

	public List<Integer> getCase_ids() {
		return case_ids;
	}

	public void setCase_ids(List<Integer> case_ids) {
		this.case_ids = case_ids;
	}
	 public int getAssignedto_id() {
		return assignedto_id;
	}

	public void setAssignedto_id(int assignedto_id) {
		this.assignedto_id = assignedto_id;
	}

	@SerializedName("assignedto_id")
	    @Expose
	    private int assignedto_id;
	

}
