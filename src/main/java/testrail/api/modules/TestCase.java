package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;


import java.util.List;

@Data
public class TestCase {

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getSectionId() {
		return sectionId;
	}
	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}
	public Integer getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public Integer getPriorityId() {
		return priorityId;
	}
	public void setPriorityId(Integer priorityId) {
		this.priorityId = priorityId;
	}
	public Object getMilestoneId() {
		return milestoneId;
	}
	public void setMilestoneId(Object milestoneId) {
		this.milestoneId = milestoneId;
	}
	public Object getRefs() {
		return refs;
	}
	public void setRefs(Object refs) {
		this.refs = refs;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Integer createdOn) {
		this.createdOn = createdOn;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Integer updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Object getEstimate() {
		return estimate;
	}
	public void setEstimate(Object estimate) {
		this.estimate = estimate;
	}
	public Object getEstimateForecast() {
		return estimateForecast;
	}
	public void setEstimateForecast(Object estimateForecast) {
		this.estimateForecast = estimateForecast;
	}
	public Integer getSuiteId() {
		return suiteId;
	}
	public void setSuiteId(Integer suiteId) {
		this.suiteId = suiteId;
	}
	public Integer getCustomAutomationType() {
		return customAutomationType;
	}
	public void setCustomAutomationType(Integer customAutomationType) {
		this.customAutomationType = customAutomationType;
	}
	public Object getCustomPreconds() {
		return customPreconds;
	}
	public void setCustomPreconds(Object customPreconds) {
		this.customPreconds = customPreconds;
	}
	public Object getCustomSteps() {
		return customSteps;
	}
	public void setCustomSteps(Object customSteps) {
		this.customSteps = customSteps;
	}
	public Object getCustomExpected() {
		return customExpected;
	}
	public void setCustomExpected(Object customExpected) {
		this.customExpected = customExpected;
	}
	public List<CustomStepsSeparated> getCustomStepsSeparated() {
		return customStepsSeparated;
	}
	public void setCustomStepsSeparated(List<CustomStepsSeparated> customStepsSeparated) {
		this.customStepsSeparated = customStepsSeparated;
	}
	public Object getCustomMission() {
		return customMission;
	}
	public void setCustomMission(Object customMission) {
		this.customMission = customMission;
	}
	public Object getCustomGoals() {
		return customGoals;
	}
	public void setCustomGoals(Object customGoals) {
		this.customGoals = customGoals;
	}
	@SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("section_id")
    @Expose
    private Integer sectionId;
    @SerializedName("template_id")
    @Expose
    private Integer templateId;
    @SerializedName("type_id")
    @Expose
    private Integer typeId;
    @SerializedName("priority_id")
    @Expose
    private Integer priorityId;
    @SerializedName("milestone_id")
    @Expose
    private Object milestoneId;
    @SerializedName("refs")
    @Expose
    private Object refs;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("created_on")
    @Expose
    private Integer createdOn;
    @SerializedName("updated_by")
    @Expose
    private Integer updatedBy;
    @SerializedName("updated_on")
    @Expose
    private Integer updatedOn;
    @SerializedName("estimate")
    @Expose
    private Object estimate;
    @SerializedName("estimate_forecast")
    @Expose
    private Object estimateForecast;
    @SerializedName("suite_id")
    @Expose
    private Integer suiteId;
    @SerializedName("custom_automation_type")
    @Expose
    private Integer customAutomationType;
    @SerializedName("custom_preconds")
    @Expose
    private Object customPreconds;
    @SerializedName("custom_steps")
    @Expose
    private Object customSteps;
    @SerializedName("custom_expected")
    @Expose
    private Object customExpected;
    @SerializedName("custom_steps_separated")
    @Expose
    private List<CustomStepsSeparated> customStepsSeparated = null;
    @SerializedName("custom_mission")
    @Expose
    private Object customMission;
    @SerializedName("custom_goals")
    @Expose
    private Object customGoals;
}
