package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;


@Data
public class Section {
	@SerializedName("name")
    @Expose
    private String name;
	
	@SerializedName("depth")
    @Expose
    private int depth;
	
	@SerializedName("description")
    @Expose
    private String description;
	
	@SerializedName("display_order")
    @Expose
    private int display_order;
	
	
	@SerializedName("id")
    @Expose
    private int id;
	
	@SerializedName("parent_id")
    @Expose
    private int parent_id;
	
	@SerializedName("suite_id")
    @Expose
    private int suite_id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDisplay_order() {
		return display_order;
	}

	public void setDisplay_order(int display_order) {
		this.display_order = display_order;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParent_id() {
		return parent_id;
	}

	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}

	public int getSuite_id() {
		return suite_id;
	}

	public void setSuite_id(int suite_id) {
		this.suite_id = suite_id;
	}
}
