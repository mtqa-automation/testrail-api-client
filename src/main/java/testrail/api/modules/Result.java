package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;


@Data
public class Result {

    @SerializedName("assignedto_id")
    @Expose
    private Integer assignedtoId;

    @SerializedName("case_id")
    @Expose
    private Integer caseId;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("created_on")
    @Expose
    private Integer createdOn;
    @SerializedName("custom_step_results")
    @Expose
    private List<CustomStepResult> customStepResults = null;
    @SerializedName("defects")
    @Expose
    private String defects;
    @SerializedName("elapsed")
    @Expose
    private String elapsed;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("status_id")
    @Expose
    private Integer status_id;
    @SerializedName("test_id")
    @Expose
    private Integer testId;
    @SerializedName("version")
    @Expose
    private String version;
	public Integer getAssignedtoId() {
		return assignedtoId;
	}
	public void setAssignedtoId(Integer assignedtoId) {
		this.assignedtoId = assignedtoId;
	}
	public Integer getCaseId() {
		return caseId;
	}
	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Integer createdOn) {
		this.createdOn = createdOn;
	}
	public List<CustomStepResult> getCustomStepResults() {
		return customStepResults;
	}
	public void setCustomStepResults(List<CustomStepResult> customStepResults) {
		this.customStepResults = customStepResults;
	}
	public String getDefects() {
		return defects;
	}
	public void setDefects(String defects) {
		this.defects = defects;
	}
	public String getElapsed() {
		return elapsed;
	}
	public void setElapsed(String elapsed) {
		this.elapsed = elapsed;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStatusId() {
		return status_id;
	}
	public void setStatusId(int statusId) {
		this.status_id = statusId;
	}
	public Integer getTestId() {
		return testId;
	}
	public void setTestId(Integer testId) {
		this.testId = testId;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
}
