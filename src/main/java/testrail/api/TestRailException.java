package testrail.api;



import org.apache.log4j.Logger;


public class TestRailException extends RuntimeException {


	final private static Logger logger = Logger.getLogger(TestRailException.class);
    public TestRailException(String message) {
    	 super(message);
    	logger.error(message);
       
    }

    public TestRailException(String message, Throwable e) {
        super(message, e);
        logger.error(message+" :"+e.getMessage());
    }


}
