package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;


@Data
public class User {

    public String getEmail() {
		return email;
	}
	public Integer getId() {
		return id;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public String getName() {
		return name;
	}
	@SerializedName("email")
    @Expose
    private String email;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("name")
    @Expose
    private String name;
}
