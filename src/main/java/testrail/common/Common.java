package testrail.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.TimeZone;

import org.apache.log4j.Logger;
import testrail.api.TestRailException;



public class Common {
	final private static Logger logger = Logger.getLogger(Common.class);
	public static void main(String[] args)
	{
		System.out.println("tested: "+Common.DateInTimeStamp("12/12/2020"));
	}

	/**
	 * Send Date with date format dd/MM/yyyy
	 * @param Date
	 * @return {@link Integer}
	 */
	public static int DateInTimeStamp(String Date) {
	//	String date = Date;
		SimpleDateFormat myDate;
		Date currentDate;
		int TS = 0;
		try {
			 myDate = new SimpleDateFormat("MM/dd/yyyy");
			myDate.setTimeZone(TimeZone.getTimeZone("GMT"));
			currentDate = myDate.parse(Date);

		//	currentDate = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH).parse(date);
			
			TS = (int) (currentDate.getTime() / 1000);
			logger.info("***"+currentDate+"*** Date Time stamp is ***"+ TS+"***");
		} catch (ParseException e) {
			currentDate = new Date();
			// TODO Auto-generated catch block
			TS = (int) (currentDate.getTime() / 1000);
			logger.error("Error occured while getting the timestamp for the "+ Date+"due to \n***"+e+"\n***");
			logger.info("***"+currentDate+"*** is the current Date and it's Time stamp is ***"+ TS+"***");
		}

		return TS;
	}
	public static boolean StringToBoolean(String bool)
	{
		boolean _status=false;
		if(bool!=null&&!bool.trim().isEmpty()&&bool.equalsIgnoreCase("true"))
			_status=true;
		
		return _status;
	}
	public static String IntToString(int value)
	{
		
		try 
		{
			return String.valueOf(value);
		}
		catch(Exception e)
		{
			logger.error("Error occured while Converting integer to String due to \n***"+e+"\n***");
			throw new TestRailException(e.getMessage(),e);
		}
	}
	public static int StringToInt(String value)
	{
		
		try 
		{
			return Integer.parseInt(value);
		}
		catch(Exception e)
		{
			logger.error("Error occured while Converting integer to String due to \n***"+e+"\n***");
			throw new TestRailException(e.getMessage(),e);
		}
	}
	
	
	
}
