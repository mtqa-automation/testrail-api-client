package testrail.api.functions;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import testrail.api.TestRailClient;
import testrail.api.TestRailClientBuilder;
import testrail.api.modules.Milestone;
import testrail.api.modules.Plan;
import testrail.api.modules.PlanEntries;
import testrail.api.modules.Project;
import testrail.api.modules.Result;
import testrail.api.modules.Run;
import testrail.api.modules.Suite;
import testrail.api.modules.Test;
import testrail.common.Common;

public class TestRail {
	final private static Logger logger = Logger.getLogger(TestRail.class);
	private static TestRailClient testrailclient;
	private static TestRail testrail;
	public TestRail(String BaseURL, String UserName, String Password) {
		TestRailClientBuilder TRB = new TestRailClientBuilder(BaseURL, UserName, Password);
		testrailclient = TRB.build();
	}
	public static TestRail GetInstance(String BaseURL, String UserName, String Password) {
		testrail = new TestRail(BaseURL, UserName, Password);
		return testrail;
	}
	public static TestRail GetInstance() {
		if (testrail == null) {
			logger.error("TestRailClient not created - use GetInstance(BaseURL, UserName, Password)");
			throw new InvalidParameterException(
					"TestRailClient not created - use GetInstance(BaseURL, UserName, Password)");
		}
		return testrail;
	}

	// =========================================Project Details Start===================================================

	/**
	 * Get the list of projects
	 * 
	 * @return List<Project>
	 * 
	 */
	public List<Project> GetProjects() {
		try {
			return testrailclient.getProjects();
		} catch (Exception e) {
			logger.error("Error occured while Getting List of Projects: " + e);
			return null;
		}
	}

	/**
	 * Get the project details
	 * 
	 * @param ProjectName
	 * @return Project
	 */
	public Project GetProject(String ProjectName) {
		Project project = new Project();
		logger.debug("***************Entering into GetProject Method *************************");
		try {
			ListIterator<Project> projects = testrailclient.getProjects().listIterator();
			while (projects.hasNext()) {
				project = projects.next();
				if (project.getName().equalsIgnoreCase(ProjectName)) {
					logger.info("*****[" + ProjectName + "]****** project is found in Test Rail");
					return project;
				}
			}

		} catch (Exception e) {
			logger.error("Error occured while getting project Details: " + e);
		}
		logger.error("***" + ProjectName + "*** project is not found in Test Rail");
		return null;
	}

	/**
	 * Get the Project ID in integer
	 * 
	 * @param ProjectName
	 * @return Project
	 */
	public Integer GetProjectID(String ProjectName) {
		try {
			logger.debug("Entering into GetProjectID Method ");
			return GetProject(ProjectName).getId();
		} catch (Exception e) {
			logger.error("Error occured while getting projectId: " + e);
		}
		return null;
	}

	/**
	 * Update Project with completion
	 * 
	 * @param ProjectName
	 * @param IsCompleted
	 * @return Project
	 */
	public Project UpdateProject(String ProjectName, String IsCompleted) {
		logger.debug("Entering into UpdateProject Method ");
		Project project = new Project();
		try {
			boolean isCompleted = false;
			if (IsCompleted.toUpperCase().equalsIgnoreCase("TRUE"))
				isCompleted = true;
			project.setIsCompleted(isCompleted);
			project = testrailclient.updateProject(GetProjectID(ProjectName), project);
			logger.info("***" + ProjectName + "*** project is updated succesfully!");
		} catch (Exception e) {
			logger.error("Error occured while Updating project Details: " + e);
		}

		return project;
	}

	/**
	 * Add the New project if project is not present
	 * 
	 * @param ProjectName
	 * @param announcement/Description
	 * @return Project
	 */
	public Project AddProject(String ProjectName, String announcement) {
		return AddProject(ProjectName, announcement, "false", "");
	}

	/**
	 * Add a New project
	 * 
	 * @param ProjectName
	 * @param announcement
	 * @param show_announcement
	 * @param suite_mode
	 * @return Project
	 */

	private Project AddProject(String ProjectName, String announcement, String show_announcement, String suite_mode) {
		Project project = new Project();
		int SuiteMode = 3;
		logger.debug("********************Entering into Add Project Method***********************");
		try {
			if (GetProject(ProjectName) != null) {
				logger.warn(ProjectName + ": Project is already exist");
				project = GetProject(ProjectName);
			} else {
				boolean Show_announcement = false;
				if (show_announcement.toUpperCase().equalsIgnoreCase("TRUE"))
					Show_announcement = true;
				project.setName(ProjectName);
				project.setAnnouncement(announcement);
				project.setShowAnnouncement(Show_announcement);
				if (suite_mode != null && !suite_mode.isEmpty() && !suite_mode.equalsIgnoreCase("empty"))
					SuiteMode = Integer.parseInt(suite_mode);
				project.setSuiteMode(SuiteMode);
				project = testrailclient.addProject(project);
				logger.info("***" + ProjectName + "***Project is Added to test rail");
			}
			logger.debug("********************Exiting from Add Project Method***********************");
			return project;
		} catch (Exception e) {
			logger.error("Error occured while Adding project Details: " + e);
		}
		return project;
	}

	@SuppressWarnings("unused")
	private boolean DeleteProject(String ProjectName) {
		try {
			int projectid=GetProjectID(ProjectName);
			testrailclient.deleteProject(projectid);
			return true;
		}
		catch(Exception e)
		{
			logger.error("Error occured while Deleting project Details: " + e);
		}
		return false;
	}

	// =========================================Project Details End===================================================

	// =========================================MileStones Details Starts===================================================

	/**
	 * get the list of milestones from the project
	 * 
	 * @param ProjectName
	 * @return List<Milestone>
	 */
	private List<Milestone> GetMileStones(String ProjectName) {
		logger.debug("Entering into GetMileStonesList Method ");
		try {
			int projectID = GetProjectID(ProjectName);
			return testrailclient.getMilestones(projectID);
		} catch (Exception e) {
			logger.error("Error occured while Getting Milestone: " + e);
			return null;
		}

	}

	/**
	 * Get the milestone details from project
	 * 
	 * @param ProjectName
	 * @param MilestoneName
	 * @return Milestone
	 */
	public Milestone GetMilestone(String ProjectName, String MilestoneName) {
		Milestone milestone = new Milestone();
		try {
			logger.debug("Entering into GetMilestone Method ");
			ListIterator<Milestone> ListMilestones = GetMileStones(ProjectName).listIterator();
			while (ListMilestones.hasNext()) {
				milestone = ListMilestones.next();
				if (milestone.getName().equalsIgnoreCase(MilestoneName))
					return milestone;
			}
			logger.info("***" + MilestoneName + "***Milestone is not found in project*** " + ProjectName + "***");
		} catch (Exception e) {
			logger.error("Error occured while Getting Milestone: " + e);
		}
		return null;
	}

	/**
	 * Get the milestone id
	 * 
	 * @param Project
	 * @param MileStoneName
	 * @return int
	 */
	public int GetMilestoneID(String Project, String MileStoneName) {
		logger.debug("Entering into GetMilestoneID Method ");
		return GetMilestone(Project, MileStoneName).getId();
	}

	/**
	 * Add new Milestone in project if Milestone is not created
	 * 
	 * @param ProjectName
	 * @param MileStoneName
	 * @param MileStoneDescription
	 * @return
	 */
	public Milestone AddMileStone(String ProjectName, String MileStoneName, String MileStoneDescription) {
		Milestone milestone = new Milestone();
		try {
			logger.debug("Entering into AddMileStone Method ");
			if (GetMilestone(ProjectName, MileStoneName) != null) {
				logger.warn("***" + MileStoneName + "***MileStone is already exist");
				milestone = GetMilestone(ProjectName, MileStoneName);
			} else {
				milestone.setName(MileStoneName);
				milestone.setDescription(MileStoneDescription);
				int projectid = GetProjectID(ProjectName);
				milestone = testrailclient.addMilestone(projectid, milestone);
				logger.info("***" + MileStoneName + "*** milestone is added successfully into project " + ProjectName);
				return milestone;
			}
		} catch (Exception e) {
			logger.error("Error occured while Adding Milestone: " + e);
			milestone = null;
		}
		return milestone;
	}

	/**
	 * @param ProjectName
	 * @param MileStoneName
	 * @param IsCompleted
	 *            (boolean value)
	 * @param IsStared
	 *            (boolean value)
	 * @param StartedOn
	 *            (Date)
	 * @param CompletionOn
	 *            (Date)
	 * @return Milestone
	 */
	public Milestone UpdateMileStone(String ProjectName, String MileStoneName, String IsCompleted, String IsStared,
			String StartedOn, String CompletionOn) {
		Milestone milestone = new Milestone();
		try {
			logger.debug("Entering into UpdateMileStone Method ");

			int Milestoneid = GetMilestoneID(ProjectName, MileStoneName);
			milestone.setName(MileStoneName);
			if (IsStared != null && !IsStared.isEmpty() && !IsStared.equalsIgnoreCase("empty"))
				milestone.setIsStarted(Common.StringToBoolean(IsStared));
			if (IsCompleted != null && !IsCompleted.isEmpty() && !IsCompleted.equalsIgnoreCase("empty"))
				milestone.setIsCompleted(Common.StringToBoolean(IsCompleted));
			if (StartedOn != null && !StartedOn.isEmpty() && !StartedOn.equalsIgnoreCase("empty"))
				milestone.setStartedOn(Common.DateInTimeStamp(StartedOn));
			if (CompletionOn != null && !CompletionOn.isEmpty() && !CompletionOn.equalsIgnoreCase("empty"))
				milestone.setCompletedOn(Common.DateInTimeStamp(CompletionOn));
			milestone = testrailclient.updateMilestone(Milestoneid, milestone);
			logger.info(MileStoneName + " milestone is updated successfully!");
		} catch (Exception e) {
			logger.error("Error occured while Updating Milestone: " + e);
			milestone = null;
		}
		return milestone;
	}

	@SuppressWarnings("unused")
	private boolean DeleteMileStone(String ProjectName, String MileStoneName) {
		logger.debug("Entering into DeleteMileStone Method ");
		try {
		testrailclient.DeleteMilestone(GetMilestoneID(ProjectName, MileStoneName));
		return true;
		}
		catch(Exception e)
		{
		return false;	
		}
		
	}
	// =========================================MileStones Details End===================================================

	// =========================================TestPlans Details Start===================================================

	/**
	 * Get the list of test plans from the project
	 * 
	 * @param ProjectName
	 * @return List<Plan>
	 */
	private List<Plan> GetTestPlansList(String ProjectName) {
		logger.debug("Entering into GetTestPlansList Method ");
		try {
			int projectID = GetProjectID(ProjectName);
			return testrailclient.getPlans(projectID);
		} catch (Exception e) {
			logger.error("Error occured while Getting Test Plan List: " + e);
			return null;
		}
	}

	/**
	 * Get the test plan details
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @return Plan
	 */
	public Plan GetTestPlan(String ProjectName, String PlanName) {
		Plan plan = new Plan();
		try {
			logger.debug("Entering into GetTestPlan Method ");
			ListIterator<Plan> Plansdetails = GetTestPlansList(ProjectName).listIterator();
			while (Plansdetails.hasNext()) {
				plan = Plansdetails.next();
				if (plan.getName().equalsIgnoreCase(PlanName)) {
					logger.info("[" + PlanName + "] Test plan is found in Project>> " + ProjectName);
					return plan;
				}
			}
			logger.warn("[" + PlanName + "] Test plan is not found in Project>> " + ProjectName);
		} catch (Exception e) {
			logger.error("Error occured while Getting Test Plan: " + e);
		}
		return null;
	}

	/**
	 * Get the test plan ID
	 * 
	 * @param Project
	 * @param PlanName
	 * @return int
	 */
	public int GetTestPlanID(String Project, String PlanName) {
		logger.debug("Entering into GetTestPlanID Method ");
		return GetTestPlan(Project, PlanName).getId();
	}

	/**
	 * Create new test plan
	 * 
	 * @param ProjectName
	 * @param MilestoneName
	 * @param TestPlanName
	 * @param PlanDescription
	 * @return Plan
	 */
	public Plan AddTestPlan(String ProjectName, String MilestoneName, String TestPlanName, String PlanDescription) {
		Plan plan = new Plan();
		try {
			logger.debug("Entering into GetTestPlanID Method ");
			if (GetTestPlan(ProjectName, TestPlanName) != null) {
				plan = GetTestPlan(ProjectName, TestPlanName);
				logger.warn(TestPlanName + " : test plan is already exist in the project : " + ProjectName);
			} else {
				int projectid = GetProjectID(ProjectName);
				int milestoneid = GetMilestoneID(ProjectName, MilestoneName);
				plan.setName(TestPlanName);
				plan.setDescription(PlanDescription);
				plan.setMilestoneId(milestoneid);
				plan = testrailclient.addPlan(projectid, plan);
				logger.info(TestPlanName + " : test plan is added successfully in the project : " + ProjectName);
			}
		} catch (Exception e) {
			logger.error("Error occured while Getting Test Plan with ID: " + e);
		}
		return plan;
	}

	@SuppressWarnings("unused")
	private boolean DeleteTestPlan(String Project, String PlanName) {
		logger.debug("Entering into GetTestPlanID Method ");
		try {
		testrailclient.deletePlan( GetTestPlan(Project, PlanName).getId());
		return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	// =========================================TestPlans Details End===================================================

	// =========================================PlanEntry Details from Plans Start===================================================
	/**
	 * Get list of Plan Entry/Test run
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @return List<PlanEntries>
	 */
	public List<PlanEntries> GetPlanEntrysList(String ProjectName, String PlanName) {
		logger.debug("Entering into GetPlanEntrysList Method ");
		try {
			return testrailclient.getPlan(GetTestPlanID(ProjectName, PlanName)).getEntries();
		} catch (Exception e) {
			logger.error("Error occured while Getting List of PlanEntry: " + e);
			return null;
		}
	}

	/**
	 * Get the Plan Entry /Test run details
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @param PlanEntryName
	 * @return PlanEntries
	 */
	public PlanEntries GetPlanEntry(String ProjectName, String PlanName, String PlanEntryName) {
		logger.debug("Entering into GetPlanEntry Method ");
		PlanEntries planentry = new PlanEntries();
		try {
			ListIterator<PlanEntries> PlanentryList = GetPlanEntrysList(ProjectName, PlanName).listIterator();
			while (PlanentryList.hasNext()) {
				planentry = PlanentryList.next();
				if (planentry.getName().equalsIgnoreCase(PlanEntryName))
					return planentry;
			}
			logger.warn(PlanEntryName + " :Plan Entry is not found in plan: " + PlanName);
		} catch (Exception e) {
			logger.error("Error occured while Getting PlanEntry: " + e);
		}
		return null;
	}

	/**
	 * Get the Plan Entry ID
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @param PlanEntryName
	 * @return
	 */

	public String GetPlanEntryID(String Project, String PlanName, String EntryName) {
		logger.debug("Entering into GetPlanEntryID Method ");
		try {
			String Id = GetPlanEntry(Project, PlanName, EntryName).getId();
			logger.info(EntryName + " Plan entry id is :" + Id);
			return Id;
		} catch (Exception e) {
			logger.error("Error occured while Getting PlanEntry ID : " + e);
			return null;
		}
	}

	/**
	 * Set the Plan Entry object details
	 * 
	 * @param ProjectName
	 * @param PlanentryName
	 * @param SuiteName
	 * @param TestCaseID
	 * @param IsincludeAllTC
	 * @return PlanEntries
	 */
	private PlanEntries SetPlanEntry(String ProjectName, String PlanentryName, String SuiteName,
			List<Integer> TestCaseID, String IsincludeAllTC,String emailid) {
		logger.debug("Entering into GetPlanEntryID Method ");
		PlanEntries planentry = new PlanEntries();
		planentry.setName(PlanentryName);
		planentry.setAssignedto_id(GetCurrentUserID(emailid));
		if (SuiteName != null && !SuiteName.isEmpty())
			planentry.setSuite_id(GetSuiteID(ProjectName, SuiteName));
		if (Common.StringToBoolean(IsincludeAllTC))
			planentry.setInclude_all(true);
		else {
			planentry.setCase_ids(TestCaseID);

		}
		return planentry;

	}

	/**
	 * Create new plan entry
	 * 
	 * @param ProjectName
	 * @param TestPlanName
	 * @param PlanEntryName
	 * @param SuiteName
	 * @param TestCaseIDs
	 * @param IsincludeAllTC
	 * @param IsDuplicateAllowed
	 * @return
	 */
	private PlanEntries AddNewPlanEntry(String ProjectName, String TestPlanName, String PlanEntryName, String SuiteName,
			String TestCaseIDs, String IsincludeAllTC, boolean IsDuplicateAllowed,String emailid) {
		PlanEntries planentry = new PlanEntries();
		try {
			logger.debug("Entering into GetTestPlanID Method ");
			if (GetPlanEntry(ProjectName, TestPlanName, PlanEntryName) != null && !IsDuplicateAllowed) {
				logger.warn(PlanEntryName + " : is already exist in the Plan [" + TestPlanName
						+ " ] And Please Use method 'AddDuplicatePlanEntry' for deplicate Names ");

			} else {
				int testplanid = GetTestPlanID(ProjectName, TestPlanName);
				String[] arrayofTCs = TestCaseIDs.split(",");
				List<Integer> ListTCids = new ArrayList<Integer>();
				for (String TCids : arrayofTCs) {
					TCids = TCids.replaceAll("[a-zA-Z]", "").trim();
					ListTCids.add(Integer.parseInt(TCids));
				}
				planentry = SetPlanEntry(ProjectName, PlanEntryName, SuiteName, ListTCids, IsincludeAllTC,emailid);
				Plan plan = new Plan();
				List<PlanEntries> listplanentry = new ArrayList<PlanEntries>();
				listplanentry.add(planentry);
				plan.setEntries(listplanentry);
				planentry = testrailclient.addPlanEntry(testplanid, planentry);
				logger.info(PlanEntryName + " Plan entry is added successfully!");
				return planentry;
			}

		} catch (Exception e) {
			logger.error("Error occured while Adding NewPlanEntry: " + e);
		}
		return null;

	}

	/**
	 * Add unique new Plan Entry
	 * 
	 * @param ProjectName
	 * @param TestPlanName
	 * @param TestRunName
	 * @param SuiteName
	 * @param TestCaseIDs
	 * @param IsincludeAllTC
	 * @return PlanEntries
	 */
	public PlanEntries AddNewPlanEntry(String ProjectName, String TestPlanName, String TestRunName, String SuiteName,
			String TestCaseIDs, String IsincludeAllTC,String emailid) {
	return	AddNewPlanEntry(ProjectName, TestPlanName, TestRunName, SuiteName, TestCaseIDs, IsincludeAllTC,false, emailid);
	}

	/**
	 * Add Unique new plan entry not including all the test cases
	 * 
	 * @param ProjectName
	 * @param TestPlanName
	 * @param TestRunName
	 * @param SuiteName
	 * @param TestCaseIDs
	 * @return PlanEntries
	 */
	public PlanEntries AddNewPlanEntry(String ProjectName, String TestPlanName, String TestRunName, String SuiteName,
			String TestCaseIDs,String emailid) {
		return AddNewPlanEntry(ProjectName, TestPlanName, TestRunName, SuiteName, TestCaseIDs, "false", false,emailid);
	}

	/**
	 * Add Unique new plan entry not including all the test cases from Default suite 
	 * 
	 * @param ProjectName
	 * @param TestPlanName
	 * @param TestRunName
	 * @param TestCaseIDs
	 * @return PlanEntries
	 */
	public PlanEntries AddNewPlanEntry(String ProjectName, String TestPlanName, String TestRunName,
			String TestCaseIDs,String emailid) {
		return AddNewPlanEntry(ProjectName, TestPlanName, TestRunName, GetDefaultSuite(ProjectName), TestCaseIDs, "false", false,emailid);
	}
	/**
	 * Allow to add duplicate plan entry without including all test cases
	 * 
	 * @param ProjectName
	 * @param TestPlanName
	 * @param TestRunName
	 * @param SuiteName
	 * @param TestCaseIDs
	 * @return PlanEntries
	 */
	public PlanEntries AddDuplicatePlanEntry(String ProjectName, String TestPlanName, String TestRunName, String SuiteName,
			String TestCaseIDs,String emailid) {
		logger.debug("Entering into AddDuplicatePlanEntry Method ");
		return AddNewPlanEntry(ProjectName, TestPlanName, TestRunName, SuiteName, TestCaseIDs, "false", true,emailid);
	}

	/**
	 * Update the Plan Entry
	 * 
	 * @param ProjectName
	 * @param TestPlanName
	 * @param TestRunName
	 * @param TestCaseIDs
	 * @return PlanEntries
	 */
	public PlanEntries UpdatePlanEntry(String ProjectName, String TestPlanName, String PlanEntry, String TestCaseIDs,String emailid) {

		PlanEntries planentry=new PlanEntries();
		try {
			logger.debug("Entering into AddTestCaseToPlanEntry Method ");
			int PlanID = GetTestPlanID(ProjectName, TestPlanName);
			String PlanEntryID = GetPlanEntryID(ProjectName, TestPlanName, PlanEntry);

			String[] arrayofTCs = TestCaseIDs.split(",");
			List<Integer> ListTCids = new ArrayList<Integer>();
			for (String TCids : arrayofTCs) {
				TCids = TCids.replaceAll("[a-zA-Z]", "").trim();
				ListTCids.add(Integer.parseInt(TCids));
			}
			planentry = SetPlanEntry(ProjectName, TestPlanName, null, ListTCids, "false",emailid);
			planentry=testrailclient.UpdatePlanEntry(PlanID, PlanEntryID, planentry);
			logger.info(PlanEntry + " : Plan entry is updated successfully!");
		} catch (Exception e) {
			logger.error("Error occured while Updating test cases to PlanEntry: " + e);
			planentry=null;
		}
		return planentry;
	}

	/**
	 * Update New Cases to existing Plan entry Case list
	 * 
	 * @param ProjectName
	 * @param TestPlanName
	 * @param PlanEntry
	 * @param TestCaseIDs
	 * @return boolean
	 */
	public boolean UpdateNewCaseToPlanEntry(String ProjectName, String TestPlanName, String PlanEntry,
			String TestCaseIDs,String emailid) {

		boolean _status = false;
		try {
			logger.debug("Entering into UpdateNewCaseToPlanEntry Method ");
			int PlanID = GetTestPlanID(ProjectName, TestPlanName);
			PlanEntries planentry = GetPlanEntry(ProjectName, TestPlanName, PlanEntry);
			String PlanEntryID = planentry.getId();
			String[] arrayofTCs = TestCaseIDs.split(",");
			List<Integer> ListTCids = GetTestCaseIDFromRun(ProjectName, TestPlanName, PlanEntry);
			for (String TCids : arrayofTCs) {
				TCids = TCids.replaceAll("[a-zA-Z]", "").trim();
				ListTCids.add(Integer.parseInt(TCids));
			}
			ListTCids = ListTCids.stream().distinct().collect(Collectors.toList());
			planentry = SetPlanEntry(ProjectName, PlanEntry, null, ListTCids, "false",emailid);
			testrailclient.UpdatePlanEntry(PlanID, PlanEntryID, planentry);
			logger.info(PlanEntry + " : Plan entry is updated successfully!");
			_status = true;
		} catch (Exception e) {
			logger.error("Error occured while Updating test cases to PlanEntry: " + e);
		}
		return _status;
	}

	// =========================================PlanEntry Details from Plans End===================================================
	// =========================================TestRun Details from Plans Start===================================================
	/**
	 * Get the List of Test Runs from the Test plan
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @return List<Run
	 */
	public List<Run> GetPlanRunsList(String ProjectName, String PlanName) {
		logger.debug("Entering into GetPlanRunsList Method ");
		try {
			ListIterator<PlanEntries> Entrys = testrailclient.getPlan(GetTestPlanID(ProjectName, PlanName)).getEntries()
					.listIterator();
			List<Run> runs = new ArrayList<Run>();
			while (Entrys.hasNext()) {
				runs.add(Entrys.next().getRuns().get(0));
			}
			return runs;
		} catch (Exception e) {
			logger.error("Error occured while Getting PlanRunsList: " + e);
			return null;
		}
	}
	
	/**
	 * Get the test runs from project level
	 * @author smallika
	 *
	 * @param ProjectName
	 * @return List<Run>
	 * 
	 */
	public List<Run> GetTestRuns(String ProjectName) {
		logger.debug("Entering into GetPlanRunsList Method ");
		try {
			int projectId=GetProjectID(ProjectName);
			List<Run> Testruns = testrailclient.getRuns(projectId);
			return Testruns;
		} catch (Exception e) {
			logger.error("Error occured while Getting PlanRunsList: " + e);
			return null;
		}
	}

	public Run GetTestRun(String ProjectName,String TestRunName) {
		logger.debug("Entering into GetPlanRunsList Method ");
		try {
			int projectId=GetProjectID(ProjectName);
			List<Run> Testruns = testrailclient.getRuns(projectId);
			Iterator<Run> runIterator = Testruns.iterator();
			while(runIterator.hasNext())
			{
				Run run=runIterator.next();
				if(run.getName().equalsIgnoreCase(TestRunName))
					return run;
			}
		} catch (Exception e) {
			logger.error("Error occured while Getting PlanRunsList: " + e);
		}
		return null;
	}

	/**
	 * Get the Test Run from Test Plan
	 * 
	 * @param Project
	 * @param PlanName
	 * @param RunName
	 * @return
	 */
	public Run GetPlanRun(String Project, String PlanName, String RunName) {
		logger.debug("Entering into GetPlanRun Method ");
		try {
			ListIterator<Run> listRuns = GetPlanRunsList(Project, PlanName).listIterator();
			Run run = new Run();
			while (listRuns.hasNext()) {
				run = listRuns.next();
				if (run.getName().equalsIgnoreCase(RunName))
					return run;
			}
			logger.info(RunName + " test Run is not found in Plan " + PlanName);
		} catch (Exception e) {
			logger.error("Error occured while Getting PlanRun: " + e);
		}
		return null;
	}

	/**
	 * Set Run object details
	 * 
	 * @param RunName
	 * @param TestCaseID
	 * @param IsincludeAllTC
	 * @return
	 */
	@SuppressWarnings("unused")
	private Run SetRun(String RunName, String TestCaseID, String IsincludeAllTC,String emailid)

	{
		logger.debug("Entering into SetRun Method ");
		Run run = new Run();
		run.setName(RunName);
		run.setAssignedtoId(GetCurrentUserID(emailid));
		if (Common.StringToBoolean(IsincludeAllTC))
			run.setIncludeAll(true);
		else {
			String[] arrayofTCs = TestCaseID.split(",");
			List<Integer> ListTCids = new ArrayList<Integer>();
			for (String TCids : arrayofTCs) {
				TCids = TCids.replaceAll("[a-zA-Z]", "").trim();
				ListTCids.add(Integer.parseInt(TCids));
			}
			run.setCaseIds(ListTCids);
		}

		return run;
	}
	private Run SetRunforMilestone(int suiteID,int milstoneid,String RunName, String TestCaseID, String IsincludeAllTC,String emailid)

	{
		logger.debug("Entering into SetRun Method ");
		Run run = new Run();
		run.setName(RunName);
		run.setSuiteId(suiteID);
		run.setMilestoneId(milstoneid);
		run.setAssignedtoId(GetCurrentUserID(emailid));
		if (Common.StringToBoolean(IsincludeAllTC))
			run.setIncludeAll(true);
		else {
			String[] arrayofTCs = TestCaseID.split(",");
			List<Integer> ListTCids = new ArrayList<Integer>();
			for (String TCids : arrayofTCs) {
				TCids = TCids.replaceAll("[a-zA-Z]", "").trim();
				ListTCids.add(Integer.parseInt(TCids));
				run.setIncludeAll(false);
			}
			run.setCaseIds(ListTCids);
		}

		return run;
	}

	/**
	 * Get the List of case ids from the test run
	 * 
	 * @param Project
	 * @param PlanName
	 * @param RunName
	 * @return List<Integer>
	 */
	public List<Integer> GetCaseIDFromRun(String Project, String PlanName, String RunName) {
		logger.debug("Entering into GetCaseIDFromRun Method ");
		ListIterator<Test> RunTests = GetTestList(Project, PlanName, RunName).listIterator();
		List<Integer> ListOfTCids = new ArrayList<Integer>();
		while (RunTests.hasNext()) {
			ListOfTCids.add(RunTests.next().getCaseId());
		}
		return ListOfTCids;
	}
	private Run AddTestRunforMileStone(String ProjectName, String MileStone, String RunName,
			String TestCaseIDs, String IsincludeAllTC,String emailid) {
		Run newRun = new Run();
		try {
			logger.debug("Entering into AddTestRunforMileStone Method ");
					
			if(GetTestRun(ProjectName, RunName)!=null) {
				logger.debug(RunName+" -->Test Run is already exist in the Project:"+ProjectName);
				return GetTestRun(ProjectName, RunName);
			} else {
				int projectid=GetProjectID(ProjectName);
				int milestoneid = GetMilestoneID(ProjectName, MileStone);
				int suiteID = GetSuiteList(ProjectName).get(0).getId();
				newRun = SetRunforMilestone(suiteID,milestoneid, RunName, TestCaseIDs, IsincludeAllTC,emailid);
				return testrailclient.addRun(projectid, newRun);
			}

		} catch (Exception e) {
			logger.error("Error occured while Adding Test run: " + e);
		}
		return null;
	}
	 public Run AddTestRunToMileStone(String ProjectName, String MileStone, String RunName,
			String TestCaseIDs, String IsincludeAllTC,String emailid)
	 {
		 return AddTestRunforMileStone(ProjectName, MileStone, RunName, TestCaseIDs, IsincludeAllTC,emailid);
	 }
	// =========================================TestRun Details from Plans End===================================================

	// =========================================Tests Details from Plans Start===================================================

	/**
	 * Get the Tests list from the test run
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @param RunName
	 * @return List<Test>
	 */
	public List<Test> GetTestList(String ProjectName, String PlanName, String RunName) {
		try {
			logger.debug("Entering into GetCaseIDFromRun Method ");
			int RunID = GetPlanRun(ProjectName, PlanName, RunName).getId();
			return testrailclient.getTests(RunID);
		} catch (Exception e) {
			logger.error("Error occured while Getting Test List: " + e);
			return null;
		}

	}

	/**
	 * Get the Test IDs and Case ID list in Map from the test run
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @param RunName
	 * @return Map<Integer, Integer>
	 */
	public List<Integer> GetTestCaseIDFromRun(String ProjectName, String PlanName, String RunName) {

		List<Integer> tests = new ArrayList<Integer>();
		try {
			logger.debug("Entering into GetTestIDAndCaseID Method ");
			ListIterator<Test> Tests = GetTestList(ProjectName, PlanName, RunName).listIterator();
			while (Tests.hasNext()) {
				Test test = Tests.next();
				tests.add(test.getCaseId());
			}
			return tests;
		}

		catch (Exception e) {
			logger.error("Error occured while Getting Test and Case Id list: " + e);
			return null;
		}

	}

	/**
	 * Get the TestID from testRun using CaseID
	 * 
	 * @param Project
	 * @param PlanName
	 * @param RunName
	 * @param CaseID
	 * @return test ID in int
	 */
	public Integer GetTestID(String Project, String PlanName, String RunName, int CaseID) {
		try {
			logger.debug("Entering into GetTestID Method ");
			ListIterator<Integer> runs = GetTestCaseIDFromRun(Project, PlanName, RunName).listIterator();
			int temp = 0;
			while (runs.hasNext()) {
				temp = runs.next();
				if (temp == CaseID)
					return temp;
			}
		} catch (Exception e) {
			logger.error("Error occured while Getting Test ID : " + e);
		}
		return null;

	}

	// =========================================Tests Details from Plans End===================================================

	// =========================================Test Suite Details Start===================================================

	/**
	 * Get the List of suites
	 * 
	 * @param projectName
	 * @return
	 */
	private List<Suite> GetSuiteList(String projectName) {
		logger.debug("Entering into GetSuiteList Method ");
		try {
			int projectid = GetProjectID(projectName);
			return testrailclient.getSuites(projectid);

		} catch (Exception e) {
			logger.error("Error occured while Getting Test ID : " + e);
			return null;
		}
	}

	public Suite GetSuite(String Project, String SuiteName) {
		logger.debug("Entering into GetSuite Method ");
		try {
			ListIterator<Suite> Listsuite = GetSuiteList(Project).listIterator();
			Suite suite = new Suite();
			while (Listsuite.hasNext()) {
				suite = Listsuite.next();
				if (suite.getName().equalsIgnoreCase(SuiteName))
					return suite;
			}
			logger.info(SuiteName + " :Suite is not found in Project: " + Project);
		} catch (Exception e) {
			logger.error("Error occured while Getting Suite: " + e);
		}
		return null;
	}

	/**
	 * Get the Suite
	 * 
	 * @param projectName
	 * @param SuiteName
	 * @return
	 */
	public Integer GetSuiteID(String projectName, String SuiteName) {

		try {
			return GetSuite(projectName, SuiteName).getId();
		} catch (Exception e) {
			logger.error("Error occured while Getting Suite ID: " + e);
			return null;
		}
	}

	private String GetDefaultSuite(String projectName) {

		return GetSuiteList( projectName).get(0).getName();
	}

	// =========================================Test Suite Details End===================================================

	// =========================================Test Result Details Start===================================================

	/**
	 * Add the Test result for Single case in test run
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @param RunName
	 * @param TestCase
	 * @param TestStatus
	 * @param Comment
	 * @param Version
	 * @param Defect
	 * @return boolean
	 */
	public boolean AddTestResult(String ProjectName, String PlanName, String RunName, String TestCase,
			TestStatus TestStatus, String Comment, String Version, String Defect) {
		boolean _status = false;
		try {
			logger.debug("Entering into AddResult Method ");
			int RunId = GetPlanRun(ProjectName, PlanName, RunName).getId();
			int TCid = Common.StringToInt(TestCase.replaceAll("[a-zA-Z]", "").trim());
			Result result = SetResult(TestStatus, Comment, Version, Defect);
			testrailclient.addResultsForCase(RunId, TCid, result);
			logger.info("Test Result is added successfully to test case: " + TestCase);
			_status = true;
		} catch (Exception e) {
			logger.error("Error occured while Adding TestResult: " + e);
		}
		return _status;

	}

	/**
	 * Add the test result for the multiple cases
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @param RunName
	 * @param TestCases
	 * @param TestStatus
	 * @param Comment
	 * @return boolean
	 */
	public boolean AddTestResult(String ProjectName, String PlanName, String RunName, String TestCases,
			TestStatus TestStatus, String Comment) {
		boolean _status = false;
		try {
			logger.debug("Entering into AddTestResult Method ");
			String[] ArrayofTestCases = TestCases.split(",");
			for (String TC : ArrayofTestCases) {
				_status = AddTestResult(ProjectName, PlanName, RunName, TC, TestStatus, Comment, "", "");
			}
		} catch (Exception e) {
			logger.error("Error occured while Adding TestResult: " + e);
		}
		return _status;
	}

	/**
	 * Add Cases to Plan Entry/Run with test result
	 * 
	 * @param ProjectName
	 * @param PlanName
	 * @param RunName
	 * @param TestCases
	 * @param TestStatus
	 * @param Comment
	 * @return boolean
	 */
	public boolean AddTestResultWithCase(String ProjectName, String PlanName, String RunName, String TestCases,
			TestStatus TestStatus, String Comment,String emailid) {
		logger.debug("Entering into AddTestResultWithCase Method ");
		boolean _status = false;
		if (UpdateNewCaseToPlanEntry(ProjectName, PlanName, RunName, TestCases,emailid))
			_status = AddTestResult(ProjectName, PlanName, RunName, TestCases, TestStatus, Comment);
		return _status;
	}

	/**
	 * @param TestStatus
	 * @param Comment
	 * @param Version
	 * @param Defect
	 * @return Result
	 */
	private Result SetResult(TestStatus TestStatus, String Comment, String Version, String Defect) {
		int statusID = GetTestStatusID(TestStatus);
		Result Rslt = new Result();
		Rslt.setStatusId(statusID);
		Rslt.setComment(Comment);
		if (Version != null && !Version.trim().isEmpty())
			Rslt.setVersion(Version);
		if (Defect != null && !Defect.trim().isEmpty())
			Rslt.setVersion(Defect);
		return Rslt;
	}

	// =========================================Test Result Details End===================================================

	// =========================================Test Case Details Start===================================================

	// =========================================Test Case Details End===================================================

	// =========================================Users Details Start===================================================
	/**
	 * Get the logged in User ID in int
	 * 
	 * @return int
	 */
	public int GetCurrentUserID(String emailID) {

		return testrailclient.getUserByEmail(emailID).getId();

	}
	public static int GetTestStatusID(TestStatus status)
	{
			switch(status)
			{
			case PASS:
			return 1;
			
			case BLOCK:
			return 2;
		
			case UNTESTED: 
			return 4;
			
			case RETEST: 
				return 3;
							
			case FAIL:
			return 5;
				
			}
			return 4;
	}
	// =========================================Users Details End===================================================
	public enum TestStatus
	{
		UNTESTED, FAIL, BLOCK, PASS,RETEST}

}