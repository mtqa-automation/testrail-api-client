package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;


@Data
public class Context {
    @SerializedName("is_global")
    @Expose
    private Boolean isGlobal;
    @SerializedName("project_ids")
    @Expose
    private Object projectIds;

}
