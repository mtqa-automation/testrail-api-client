package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;


@Data
public class Plan {

    @SerializedName("assignedto_id")
    @Expose
    private Integer assignedtoId;
    @SerializedName("blocked_count")
    @Expose
    private Integer blockedCount;
    @SerializedName("completed_on")
    @Expose
    private Integer completedOn;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("created_on")
    @Expose
    private Integer createdOn;
    @SerializedName("custom_status1_count")
    @Expose
    private Integer customStatus1Count;
    @SerializedName("custom_status2_count")
    @Expose
    private Integer customStatus2Count;
    @SerializedName("custom_status3_count")
    @Expose
    private Integer customStatus3Count;
    @SerializedName("custom_status4_count")
    @Expose
    private Integer customStatus4Count;
    @SerializedName("custom_status5_count")
    @Expose
    private Integer customStatus5Count;
    @SerializedName("custom_status6_count")
    @Expose
    private Integer customStatus6Count;
    @SerializedName("custom_status7_count")
    @Expose
    private Integer customStatus7Count;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("entries")
    @Expose
    private List<PlanEntries> entries=null;
    @SerializedName("failed_count")
    @Expose
    private Integer failedCount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("is_completed")
    @Expose
    private Boolean isCompleted;
    @SerializedName("milestone_id")
    @Expose
    private Integer milestoneId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("passed_count")
    @Expose
    private Integer passedCount;
    @SerializedName("project_id")
    @Expose
    private Integer projectId;
    @SerializedName("retest_count")
    @Expose
    private Integer retestCount;
    @SerializedName("untested_count")
    @Expose
    private Integer untestedCount;
    @SerializedName("url")
    @Expose
    private String url;
	public Integer getAssignedtoId() {
		return assignedtoId;
	}
	public void setAssignedtoId(Integer assignedtoId) {
		this.assignedtoId = assignedtoId;
	}
	public Integer getBlockedCount() {
		return blockedCount;
	}
	public void setBlockedCount(Integer blockedCount) {
		this.blockedCount = blockedCount;
	}
	public Object getCompletedOn() {
		return completedOn;
	}
	public void setCompletedOn(Integer completedOn) {
		this.completedOn = completedOn;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Integer createdOn) {
		this.createdOn = createdOn;
	}
	public Integer getCustomStatus1Count() {
		return customStatus1Count;
	}
	public void setCustomStatus1Count(Integer customStatus1Count) {
		this.customStatus1Count = customStatus1Count;
	}
	public Integer getCustomStatus2Count() {
		return customStatus2Count;
	}
	public void setCustomStatus2Count(Integer customStatus2Count) {
		this.customStatus2Count = customStatus2Count;
	}
	public Integer getCustomStatus3Count() {
		return customStatus3Count;
	}
	public void setCustomStatus3Count(Integer customStatus3Count) {
		this.customStatus3Count = customStatus3Count;
	}
	public Integer getCustomStatus4Count() {
		return customStatus4Count;
	}
	public void setCustomStatus4Count(Integer customStatus4Count) {
		this.customStatus4Count = customStatus4Count;
	}
	public Integer getCustomStatus5Count() {
		return customStatus5Count;
	}
	public void setCustomStatus5Count(Integer customStatus5Count) {
		this.customStatus5Count = customStatus5Count;
	}
	public Integer getCustomStatus6Count() {
		return customStatus6Count;
	}
	public void setCustomStatus6Count(Integer customStatus6Count) {
		this.customStatus6Count = customStatus6Count;
	}
	public Integer getCustomStatus7Count() {
		return customStatus7Count;
	}
	public void setCustomStatus7Count(Integer customStatus7Count) {
		this.customStatus7Count = customStatus7Count;
	}
	public Object getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<PlanEntries> getEntries() {
		return entries;
	}
	public void setEntries(List<PlanEntries> entries) {
		this.entries = entries;
	}
	public Integer getFailedCount() {
		return failedCount;
	}
	public void setFailedCount(Integer failedCount) {
		this.failedCount = failedCount;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Boolean getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public Integer getMilestoneId() {
		return milestoneId;
	}
	public void setMilestoneId(Integer milestoneId) {
		this.milestoneId = milestoneId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPassedCount() {
		return passedCount;
	}
	public void setPassedCount(Integer passedCount) {
		this.passedCount = passedCount;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getRetestCount() {
		return retestCount;
	}
	public void setRetestCount(Integer retestCount) {
		this.retestCount = retestCount;
	}
	public Integer getUntestedCount() {
		return untestedCount;
	}
	public void setUntestedCount(Integer untestedCount) {
		this.untestedCount = untestedCount;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
