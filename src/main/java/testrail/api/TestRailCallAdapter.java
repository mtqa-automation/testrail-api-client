package testrail.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;

import org.apache.log4j.Logger;


public class TestRailCallAdapter<T> extends CallAdapter.Factory {
	final private static Logger logger = Logger.getLogger(TestRailCallAdapter.class);
    @Override
    public CallAdapter<T, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
    	logger.info("Entered into CallAdapter method TestRailCallAdapter class ");
        if (returnType.getTypeName().startsWith(Call.class.getName())) {
            return null;
        }
        if (returnType.getTypeName().startsWith(retrofit2.Response.class.getName())) {
            return new ResponseCallAdapter(((ParameterizedType) returnType).getActualTypeArguments()[0]);
        }
        return new InstanceCallAdapter(returnType);
    }

    private static String getErrorMessage( retrofit2.Response<?> response) {
    	logger.info("Entered into getErrorMessage method TestRailCallAdapter class ");
         ResponseBody errorBody = response.errorBody();
         String errorMessage;
        try {
            errorMessage = Objects.isNull(errorBody) ? response.message() : errorBody.string();
        } catch (IOException e) {
            throw new TestRailException("Could not read error body", e);
        }
        return errorMessage;
    }

    private class ResponseCallAdapter implements CallAdapter<T, retrofit2.Response<T>> {

        private  Type returnType;

        ResponseCallAdapter( Type returnType) {
        	logger.info("Entered into ResponseCallAdapter method TestRailCallAdapter class ");
            this.returnType = returnType;
        }

        public Type responseType() {
            return returnType;
        }

        public retrofit2.Response<T> adapt(Call<T> call) {
        	logger.info("Entered into adapt-Response method TestRailCallAdapter class ");
             retrofit2.Response<T> response;
            try {
                response = call.execute();
            } catch (IOException e) {
                throw new TestRailException("Could not execute request", e);
            }

            if (!response.isSuccessful()) {
                throw new TestRailException(getErrorMessage(response));
            }
            return response;
        }
    }

    private class InstanceCallAdapter implements CallAdapter<T, Object> {
        
        private  Type returnType;

        InstanceCallAdapter(Type returnType) {
        	
            this.returnType = returnType;
        }

        public Type responseType() {
            return returnType;
        }

        public Object adapt(Call<T> call) {
        	logger.info("Entered into adapt method TestRailCallAdapter class ");
             retrofit2.Response<T> response;
            try {
                response = call.execute();
            } catch (IOException e) {
                throw new TestRailException("Could not get request body", e);
            }
            if (!response.isSuccessful()) {
                throw new TestRailException(getErrorMessage(response));
            }
            return response.body();
        }
    }

}
