package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;


@Data
public class Project {

    @SerializedName("id")
    @Expose
    private int id;
    public int getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(Object announcement) {
		this.announcement = announcement;
	}
	public Boolean getShowAnnouncement() {
		return showAnnouncement;
	}
	public void setShowAnnouncement(Boolean showAnnouncement) {
		this.showAnnouncement = showAnnouncement;
	}
	public Boolean getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public Object getCompletedOn() {
		return completedOn;
	}
	public void setCompletedOn(Object completedOn) {
		this.completedOn = completedOn;
	}
	public Integer getSuiteMode() {
		return suiteMode;
	}
	public void setSuiteMode(Integer suiteMode) {
		this.suiteMode = suiteMode;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@SerializedName("name")
    @Expose
    private String name;
    @SerializedName("announcement")
    @Expose
    private Object announcement;
    @SerializedName("show_announcement")
    @Expose
    private Boolean showAnnouncement;
    @SerializedName("is_completed")
    @Expose
    private Boolean isCompleted;
    @SerializedName("completed_on")
    @Expose
    private Object completedOn;
    @SerializedName("suite_mode")
    @Expose
    private Integer suiteMode;
    @SerializedName("url")
    @Expose
    private String url;
    
}
