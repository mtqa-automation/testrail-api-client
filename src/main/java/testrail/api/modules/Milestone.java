package testrail.api.modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;


@Data
public class Milestone {

    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getStartOn() {
		return startOn;
	}
	public void setStartOn(Integer startOn) {
		this.startOn = startOn;
	}
	public Object getStartedOn() {
		return startedOn;
	}
	public void setStartedOn(Object startedOn) {
		this.startedOn = startedOn;
	}
	public Boolean getIsStarted() {
		return isStarted;
	}
	public void setIsStarted(Boolean isStarted) {
		this.isStarted = isStarted;
	}
	public Integer getDueOn() {
		return dueOn;
	}
	public void setDueOn(Integer dueOn) {
		this.dueOn = dueOn;
	}
	public Boolean getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	public Object getCompletedOn() {
		return completedOn;
	}
	public void setCompletedOn(Object completedOn) {
		this.completedOn = completedOn;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Object getParentId() {
		return parentId;
	}
	public void setParentId(Object parentId) {
		this.parentId = parentId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("start_on")
    @Expose
    private Integer startOn;
    @SerializedName("started_on")
    @Expose
    private Object startedOn;
    @SerializedName("is_started")
    @Expose
    private Boolean isStarted;
    @SerializedName("due_on")
    @Expose
    private Integer dueOn;
    @SerializedName("is_completed")
    @Expose
    private Boolean isCompleted;
    @SerializedName("completed_on")
    @Expose
    private Object completedOn;
    @SerializedName("project_id")
    @Expose
    private Integer projectId;
    @SerializedName("parent_id")
    @Expose
    private Object parentId;
    @SerializedName("url")
    @Expose
    private String url;
}
